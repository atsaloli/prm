#!/usr/bin/env python
import os
import sys
import yaml
import prm_config  # PRM configuration settings
from prm_lib import debug
from prm_lib import directory_walk

# searches every record in the PRM database
# (reads and validates every YAML file)


def main():
    if len(sys.argv) > 1:
        name = sys.argv[1]  # first argument is name to search for
    else:
        print("Error, please supply Name to search for.", file=sys.stderr)
        print("Example: search.py Aleksey", file=sys.stderr)
        sys.exit(1)
    print(search(name))


def search(name):
    # given the person's name as input,
    # go through every record in the PRM database
    # and dump records matching the name (case-insensitive search)

    for phonebook_record in directory_walk():
        debug("Checking " + phonebook_record["name"])
        if name.lower() in phonebook_record["name"].lower():
            return "---\n" + yaml.dump(phonebook_record) + "..."
        else:
            debug(name.lower() + " not in " + phonebook_record["name"].lower())


if __name__ == "__main__":
    main()
