import os

repo_dir = os.path.join(os.environ['HOME'], os.environ['PRM_REPO'])
data_dir = repo_dir + "/person-records"
schema_dir = repo_dir + "/schema"
