# prm - person record manager

prm is a glorified addressbook, it lets you keep track of people's
information like name, phone, email, address, etc.

I wanted to own my own data again, like I used to when I had this
kind of stuff in a little perl project called phonebook.pl which used
a single data structure inside phonebook.dat to keep everyone's details.

I kind of went lax and let clouds and service providers and social media
networks hold my data, but now that you can put data in but not take it
out anymore (as with social networks), I want to own my own data again.

Plus, I am learning Python and this gives me a real-world application to
practice my Python skills.

I am using the Person schema from https://schema.org/Person for the YAML
records, with some of my own extensions marked with `x-`, e.g.,
`x-facebook` for a link to the person's Facebook profile.

The schema is in `config_schema` in `prm_lib.py`.

There is a pre-commit git hook in `pre-commit` which you can install if
you want (add it to `.git/hooks/pre-commit`).

## Installation

1. Clone this repo and edit `prm_config.py` to set the path to your data
directory. This will be where prm will store the YAML files that make
up the individual records.  Add the directory where you cloned this repo
to your PATH.

2. Also in `prm_config.py`, set the path to your schema directory. Then
create a `prm_schema.py` file in that schema directory and define your
PRM schema.

Example:

```
$ cat ../prm-data/schema/prm_schema.py
"""Schema for PRM"""

from schema import Schema, And, Use, Optional, SchemaError # https://github.com/keleshev/schema

config_schema = Schema(
    {
        "name": str,
        Optional("x-prm-filename"): str,
        Optional("telephone"): str,
        Optional("facebook"): str,  # my own extension (not present in schema.org
        Optional("email"): str,
        Optional("birthDate"): str,
        Optional("x-tags"): list,
    }
)
$
```

## Usage

- `prm add` - add a record
- `prm edit` - edit a record
- `prm list` - list records
- `prm search <name>` - dump the record for a given person
- `prm validate` - validate records
- `prm birthday` - show all the birthdays for today

## Support

Open an issue in this project to get support.


## Contributing

You can open an issue to coordinate with me, or just submit a pull
request.

There is a TODO file.

## Authors

Aleksey Tsalolikhin

## License

MIT license

Copyright 2022 Aleksey Tsalolikhin

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
