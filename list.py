#!/home/linuxbrew/.linuxbrew/bin/python3

import prm_lib  # my library

# goes through every record in the PRM database
# and pulls out the `name` field

for phonebook_record in prm_lib.directory_walk():
    print(phonebook_record["name"])
