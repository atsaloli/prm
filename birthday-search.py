#!/usr/bin/env python

import yaml
import prm_lib

# searches every record in the PRM database
# (looks for birthday matches to today's date)

from datetime import date

today = date.today()

# dd/mm/YY
today_date = today.strftime(
    "-%m-%d"
)  # omit the year, I don't always know the year when someone is born

for phonebook_record in prm_lib.directory_walk():
    if "birthDate" in phonebook_record:
        if today_date in phonebook_record["birthDate"]:
            print("---")
            print(yaml.dump(phonebook_record))
            print("...")
