#!/usr/bin/env python

import prm_lib  # my library

# goes through every record in the PRM database
# and pulls out the `x-tags` field and prints each
# tag (so that we can generate a unique list of tags)

for phonebook_record in prm_lib.directory_walk():
    if "x-tags" in phonebook_record:
        if len(phonebook_record["x-tags"]) > 0:
            for tag in phonebook_record["x-tags"]:
                print(tag)
