#!/usr/bin/env python
import prm_lib  # my library

# validates every record in the PRM database

for phonebook_record in prm_lib.directory_walk():
    prm_lib.validate_record(phonebook_record)
