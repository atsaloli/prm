#!/usr/bin/env python
import os
import sys
import yaml
import prm_lib  # my library
import prm_config  # PRM configuration settings
import git

repo = git.Repo(prm_config.repo_dir)
# Adds a person record

data = {}

data["name"] = input("Name? ")
if len(data["name"]) < 1:
    print("Missing name. Aborting")
    sys.exit(1)

filename = data["name"].lower().replace(" ", "-") + ".yaml"
filepath = os.path.join(prm_config.data_dir, filename)
if os.path.exists(filepath):
    print(f"Aborting because {filename} already exists\n")
    sys.exit(1)

data["telephone"] = input("Telephone? ")
data["facebook"] = input("Facebook? ")
data["email"] = input("Email? ")
data["birthDate"] = input("Birthdate? YYYY-MM-DD format: ")
data["spouse"] = input("Spouse? ")
record_str = f"---\n"
record_str += f"name: {data['name']}\n"
if len(data["telephone"]) > 0:
    record_str += f"telephone: '{data['telephone']}'\n"
if len(data["facebook"]) > 0:
    record_str += f"facebook: {data['facebook']}\n"
if len(data["email"]) > 0:
    record_str += f"email: {data['email']}\n"
if len(data["birthDate"]) > 0:
    record_str += f"birthDate: '{data['birthDate']}'\n"
if len(data["spouse"]) > 0:
    record_str += f"spouse: '{data['spouse']}'\n"

# now let's add tags
print("Enter tags, one tag per line. Press Enter when done.")
tag_input = input("Tag? ")
if tag_input:
    # strip leading hash mark (as it triggers some bug, it makes "prm view" show the tag value as null)
    tag_input = tag_input.lstrip('#')
    record_str += f"x-tags:\n"
    record_str += f"- {tag_input}\n"

while tag_input:
    tag_input = input("Tag? ")
    if tag_input:
        record_str += f"- {tag_input}\n"

print(record_str)
print()
record_yaml = yaml.safe_load(record_str)
prm_lib.validate_record(record_yaml)
print("Validation succeeded")
with open(filepath, "x") as file:  # x will return an error if the file exists
    file.write(record_str)
print("Wrote YAML to", filepath)
print("Saving the new record to local Git repo")
repo.index.add([filepath])
repo.index.commit(f'Add record for {data["name"]}')
print("Pushing changes to remote Git repo")
repo.remotes.origin.push()
sys.exit()
