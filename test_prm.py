#!/usr/bin/env pytest
from search import search
from tag import search_by_tag
from prm_lib import validate_record
import pytest
import schema
import os


def test_search():
    assert (
        search("Test")
        == f"""---
name: Test Test
x-prm-filename: {os.environ['HOME']}/git/atsaloli/prm-data/person-records/test-test.yaml
x-tags:
- pytest
..."""
    )

def test_search_by_tag():
    for tag in search_by_tag("pytest"):
        assert (
            tag == "Test Test"
        )

def test_validate_record():
    with pytest.raises(schema.SchemaMissingKeyError):
        # there has to be a "name" key in the record
        assert validate_record({"key": "value"})
    assert validate_record({"name": "John Test"}) == None
