import os
import sys
import yaml
import prm_config
from schema import (
    Schema,
    And,
    Use,
    Optional,
    SchemaError,
)  # https://github.com/keleshev/schema

sys.path.append(prm_config.schema_dir)
import prm_schema

DEBUG = os.getenv("DEBUG")


def debug(msg):
    """Print a debug message if env var DEBUG is set"""
    if DEBUG:
        print("DEBUG: " + msg)


def directory_walk():
    with os.scandir(prm_config.data_dir) as dir_entries:
        for dir_entry in dir_entries:
            if dir_entry.is_file():
                filename = os.path.join(prm_config.data_dir, dir_entry.name)
                with open(filename) as file:
                    try:
                        prm_record = yaml.safe_load(file)
                        # append metadata: supply filename (to support
                        # edit function, and in case there is a validation
                        # error so we can see where the failure occured)
                        prm_record["x-prm-filename"] = filename
                        yield (prm_record)
                    except yaml.YAMLError as exc:
                        print(exc)
                        sys.exit(1)


def validate_record(record_yaml):
    try:
        prm_schema.config_schema.validate(record_yaml)
    except SchemaError as se:
        print("Error validating record:", record_yaml)
        raise se
        sys.exit(1)
