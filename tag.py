#!/usr/bin/env python

"""Search by tag."""

import sys
import prm_lib  # my library

# goes through every record in the PRM database
# and pulls out the `x-tags` field and returns
# names matching tag

# this is based on search.py which was originally implemented to search by name

# in contrast, this program searches by tag value



def search_by_tag(tag_value):
    for phonebook_record in prm_lib.directory_walk():
        if "x-tags" in phonebook_record:
            if len(phonebook_record["x-tags"]) > 0:
                for tag in phonebook_record["x-tags"]:
                    if tag == tag_value:
                        yield phonebook_record["name"]

def main():
  if len(sys.argv) > 1:
      tag_value = sys.argv[1]  # first argument is tag to search for
      for name in search_by_tag(tag_value):
          print(name)
  else:
      print("Error, please supply tag to search for.")
      print("Example: prm tag neighbor")
      sys.exit(1)

if __name__ == "__main__":
    main()
