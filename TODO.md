# TODO

- Extend the built-in schema to add more attributes from https://schema.org/Person and https://schema.org/address
  - Add address to the schema in validate.py and to add.py
    - See https://schema.org/PostalAddress
- add bash command completion
- add Python linting to pre-commit hook
